# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <bald@smail.ee>, 2012, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:11+0200\n"
"PO-Revision-Date: 2016-01-14 13:03+0200\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Marek Laane"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "qiilaq69@gmail.com"

#: conference-auth-op.cpp:94
#, kde-format
msgid "Please provide a password for the chat room %1"
msgstr "Palun sisesta jututoa %1 parool"

#: main.cpp:51
#, kde-format
msgid "Telepathy Authentication Handler"
msgstr "Telepathy autentimise käitleja"

#: main.cpp:54
#, kde-format
msgid "David Edmundson"
msgstr "David Edmundson"

#: main.cpp:54 main.cpp:55
#, kde-format
msgid "Developer"
msgstr "Arendaja"

#: main.cpp:55
#, kde-format
msgid "Daniele E. Domenichelli"
msgstr "Daniele E. Domenichelli"

#: tls-cert-verifier-op.cpp:89
#, kde-format
msgid "Invalid certificate type %1"
msgstr "Vigane sertifikaaditüüp %1"

#: tls-cert-verifier-op.cpp:102
#, kde-format
msgid ""
"The SSL/TLS support plugin is not available. Certificate validation cannot "
"be done."
msgstr ""
"SSL/TLS toetuse pluginat pole saadaval. Sertifikaadi õigsust ei saa "
"kontrollida."

#: tls-cert-verifier-op.cpp:120
#, kde-format
msgid "Certificate rejected by the user"
msgstr "Kasutaja lükkas sertifikaadi tagasi"

#: tls-cert-verifier-op.cpp:146
#, kde-format
msgid ""
"The server failed the authenticity check (%1).\n"
"\n"
msgstr ""
"Server ei läbinud autentsuse testi (%1)\n"
"\n"

#: tls-cert-verifier-op.cpp:156 tls-cert-verifier-op.cpp:174
#, kde-format
msgid "Server Authentication"
msgstr "Serveri autentimine"

#: tls-cert-verifier-op.cpp:157
#, kde-format
msgid "&Details"
msgstr "Üksikasja&d"

#: tls-cert-verifier-op.cpp:158
#, kde-format
msgid "Co&ntinue"
msgstr "&Jätka"

#: tls-cert-verifier-op.cpp:159
#, kde-format
msgid "&Cancel"
msgstr "&Loobu"

#: tls-cert-verifier-op.cpp:171
#, kde-format
msgid ""
"Would you like to accept this certificate forever without being prompted?"
msgstr "Kas aktsepteerida seda sertifikaati tulevikus ilma küsimata?"

#: tls-cert-verifier-op.cpp:175
#, kde-format
msgid "&Forever"
msgstr "&Jah"

#: tls-cert-verifier-op.cpp:176
#, kde-format
msgid "&Current Session only"
msgstr "&Ainult käesolevas seansis"

#: x-telepathy-password-auth-operation.cpp:125
#: x-telepathy-sso-google-operation.cpp:63
#, kde-format
msgid "Authentication error"
msgstr "Autentimise tõrge"

#: x-telepathy-password-auth-operation.cpp:144
#, kde-format
msgid "User cancelled auth"
msgstr "Kasutaja katkestas autentimise"

#. i18n: ectx: property (text), widget (KTitleWidget, title)
#: x-telepathy-password-prompt.ui:47
#, kde-format
msgid "Please Enter Your Account Password"
msgstr "Palun sisesta oma konto parool"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: x-telepathy-password-prompt.ui:59
#, kde-format
msgid "Account:"
msgstr "Konto:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: x-telepathy-password-prompt.ui:73
#, kde-format
msgid "Password:"
msgstr "Parool:"

#. i18n: ectx: property (text), widget (QCheckBox, savePassword)
#: x-telepathy-password-prompt.ui:87
#, kde-format
msgid "Save Password"
msgstr "Parooli salvestamine"

#~ msgid "Accept this certificate from <b>%1?</b><br />%2<br />"
#~ msgstr "Kas aktsepteerida <b>%1</b> sertifikaat?<br />%2 <br />"

#~ msgid "Untrusted certificate"
#~ msgstr "Ebausaldusväärne sertifikaat"
